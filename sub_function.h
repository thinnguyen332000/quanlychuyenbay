/*
Sub Functions Create by MIKE - MAI XUAN Y - PTIT
                __  _________ __ ______
               /  |/  /  _/ //_// ____/
              / /|_/ // // ,<  / __/
             / /  / // // /| |/ /___
            /_/  /_/___/_/ |_/_____/

*/

#pragma once
#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

int toInt(string s);
int toInt(char* s);

/*Function Deployment*/
int toInt(string s) {
    int num = 0;
    for (int i = 0; i < s.size(); i++) {
        num = num * 10 + s[i] - 48;
    }
    return num;
}

int toInt(char* s) {
    int num = 0;
    for (int i = 0; i < strlen(s); i++) {
        num = num * 10 + s[i] - 48;
    }
    return num;
}