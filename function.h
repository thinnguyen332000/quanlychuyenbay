/*
Processing Functions Create by MIKE - MAI XUAN Y - PTIT
                __  _________ __ ______
               /  |/  /  _/ //_// ____/
              / /|_/ // // ,<  / __/
             / /  / // // /| |/ /___
            /_/  /_/___/_/ |_/_____/

*/

#pragma once
#include <conio.h>
#include "console.h"

void Init_Main();
void Handle_Main();
void After_Main();

/*Function Deployment*/
void Init_Main() {
    /*Read File Here*/
    int x = 4;
    resizeConsole(1300, 760);
    DisableResizeWindow();
    Remove_Scrollbars();
    DisableCtrButton(0, 1, 1);
    ShowCur(false);
    Frame_Drawing(0, height, 0, width);
    Draw_A_Horizontal_Line(height - 4, 0, width);
    Draw_A_Vertical_Line(width_title, 0, height - 4);
    Draw_A_Horizontal_Line(height_title, 0, width_title);
    Draw_Name_Project();
    Draw_Button("ESC:THOAT", x, height-3);
    x = wherex() + 2;
    Draw_Button("ENTER:CHON", x, height - 3);
    x = wherex() + 2;
    Draw_Button("UP:LEN", x, height - 3);
    x = wherex() + 2;
    Draw_Button("DOWN:XUONG", x, height - 3);
    x = wherex() + 2;
    Draw_Button("PAGEUP:TRANG TRUOC", x, height - 3);
    x = wherex() + 2;
    Draw_Button("PAGEDOWN:TRANG SAU", x, height - 3);
    x = wherex() + 2;
    Draw_Button("INSERT:THEM", x, height - 3);
    x = wherex() + 2;
    Draw_Button("DELETE:XOA", x, height - 3);
    x = wherex() + 2;
    Draw_Button("CTRL+F:TIM KIEM", x, height - 3);
    x = wherex() + 2;
    Draw_Button("F4:CHINH SUA", x, height - 3);
}

void Handle_Main() {
    char input;
    while (1) {
        input = getch();
        if (input == ENTER) break;
    }
}

void After_Main() {

}
